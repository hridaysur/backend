const phrases = require('../phrases');
exports.seed = function (knex) {
    // Deletes ALL existing entries
    return Promise.all([
        knex("phrases").del()
            .then(function () {
            // Inserts seed entries
            return knex("phrases").insert(phrases);
        })
    ]);
};
//# sourceMappingURL=o1_phrase.js.map