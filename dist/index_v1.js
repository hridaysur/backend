"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Hapi = require("hapi");
const fs = require("fs");
const fs_extra = require("fs-extra");
const Loki = require("lokijs");
const promise = require("bluebird");
const readline = require("readline");
// setup
const DB_NAME = 'db.json';
const COLLECTION_NAME = 'images';
const UPLOAD_PATH = 'uploads';
const fileOptions = { dest: `${UPLOAD_PATH}/` };
const db = new Loki(`${UPLOAD_PATH}/${DB_NAME}`, { persistenceMethod: 'fs' });
promise.promisifyAll(fs);
let phrases = { "phrases": [] };
;
let line_list = [];
// create folder for upload if not exist
if (!fs.existsSync(UPLOAD_PATH))
    fs.mkdirSync(UPLOAD_PATH);
// app
const server = new Hapi.Server({
    port: 8081, host: 'localhost',
    routes: { cors: true }
});
server.route({
    method: 'GET',
    path: '/read',
    handler: (request, h) => {
        return new promise((resolve, reject) => {
            let instream = fs.createReadStream('storage1.txt');
            let r1 = readline.createInterface({
                input: instream,
                output: process.stdout
            });
            r1.on("line", (line) => {
                line_list.push(line);
            });
            r1.on("error", error => {
                reject(error);
            });
            for (let i = 0; i < line_list.length; i++) {
                console.log('the index is ' + i);
            }
            resolve(line_list);
        });
    }
});
server.route({
    method: 'POST',
    path: '/write',
    handler: (request, response) => {
        return new promise((resolve, reject) => {
            let phrase = JSON.stringify(request.payload);
            let temp_json = JSON.parse(phrase);
            let obj = { _id: Math.floor(Math.random() * 1000), phrase: temp_json.phrase };
            phrases.phrases.push(obj);
            var json = JSON.stringify(phrases, null, 4);
            fs_extra.outputJson('storage.txt', json, (err) => {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(obj._id);
                }
            });
        });
    }
});
server.route({
    method: 'GET',
    path: '/read/{_id}',
    handler: (request, response) => {
        return new promise((resolve, reject) => {
            const remove_phrase = request.params._id;
            fs_extra.readJson('storage.txt')
                .then(data => {
                // console.log(data.toString());
                let json_data = JSON.parse(data.toString());
                json_data.phrases.forEach((element, index) => {
                    if (element._id == remove_phrase) {
                        console.log('yes its there');
                        json_data.phrases.splice(index, 1);
                        fs_extra.writeJson('storage.txt', JSON.stringify(json_data), (err) => {
                            if (err) {
                                reject(err);
                            }
                            else {
                                resolve(json_data);
                            }
                        });
                    }
                    else {
                        console.log('no phrase foound');
                    }
                });
            })
                .catch(err => {
                console.error(err);
            });
            // console.log(id);
            // json.phrase = id.filter((id) => { return id._id !== remove_phrase });
            // resolve(json.phrase);
            // console.log(json);
        });
    }
});
// start our app
function start() {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield server.start();
        }
        catch (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server running at:', server.info.uri);
    });
}
;
start();
//# sourceMappingURL=index_v1.js.map