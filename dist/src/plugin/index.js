"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("../config");
const logger_1 = require("../helper/logger");
class Plugins {
    static status(server) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                logger_1.default.info('Plugins - Registering status-monitor');
                yield Plugins.register(server, {
                    path: config_1.default.status.path,
                    options: config_1.default.status.options,
                    register: require('hapijs-status-monitor'),
                });
            }
            catch (error) {
                logger_1.default.info(`Plugins - Ups, something went wrong when registering status plugin: ${error}`);
            }
        });
    }
    static boom(server) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                logger_1.default.info('Plugins - Registering hapi-boom-decorators');
                yield Plugins.register(server, {
                    register: require('hapi-boom-decorators'),
                });
            }
            catch (error) {
                logger_1.default.info(`Plugins - Ups, something went wrong when registering hapi-boom-decorators plugin: ${error}`);
            }
        });
    }
    static registerAll(server) {
        return __awaiter(this, void 0, void 0, function* () {
            if (process.env.NODE_ENV === 'development') {
                yield Plugins.status(server);
            }
            yield Plugins.boom(server);
        });
    }
    static register(server, plugin) {
        return new Promise((resolve, reject) => {
            server.register(plugin);
        });
    }
}
exports.default = Plugins;
//# sourceMappingURL=index.js.map