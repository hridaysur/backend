"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const resolver_1 = require("../common/resolver");
const repository_1 = require("../common/repository");
class PhraseResolver extends resolver_1.default {
    constructor() {
        super(new repository_1.default());
    }
}
exports.default = PhraseResolver;
//# sourceMappingURL=resolver.js.map