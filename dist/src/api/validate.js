"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Joi = require("joi");
exports.default = {
    create: {
        payload: {
            phrase: Joi.string().required()
        },
    }
};
//# sourceMappingURL=validate.js.map