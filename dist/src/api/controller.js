"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const resolver_1 = require("./resolver");
const crud_controller_1 = require("../common/crud-controller");
class PhraseController extends crud_controller_1.default {
    constructor() {
        super(new resolver_1.default());
    }
}
exports.default = PhraseController;
//# sourceMappingURL=controller.js.map