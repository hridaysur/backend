"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = require("../helper/logger");
const controller_1 = require("./controller");
class UserRoutes {
    register(server) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(resolve => {
                logger_1.default.info('Phrase - Start adding Phrase routes.');
                const controller = new controller_1.default();
                server.route([
                    {
                        method: 'POST',
                        path: '/write',
                        handler: controller.create,
                    },
                    {
                        method: 'GET',
                        path: '/read',
                        handler: controller.getAll,
                    },
                    {
                        method: 'DELETE',
                        path: '/delete/{_id}',
                        handler: controller.deleteByID
                    }
                ]);
                logger_1.default.info('Phrase - Finish adding Phrase routes.');
                resolve();
            });
        });
    }
}
exports.default = UserRoutes;
//# sourceMappingURL=routes.js.map