"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Utils {
    static getUrl(request) {
        request;
        return `${request.server.info.protocol}://${process.env.HOST}:${process.env.PORT}${request.url.path}`;
    }
}
exports.default = Utils;
//# sourceMappingURL=utils.js.map