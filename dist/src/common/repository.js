"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs-extra");
const promise = require("bluebird");
const readline = require("readline");
var quiry = require('../db/quaries');
promise.promisifyAll(fs);
let phrases = [];
let line_list = [];
class Repository {
    save(data) {
        return new Promise((resolve, reject) => {
            let temp_json = JSON.parse(data);
            let ph = `${temp_json.phrase}`;
            fs.readFile('storage.txt', "utf8", function (err, data) {
                if (err) {
                    reject(err);
                }
                var textbyline = data.split('\n');
                textbyline.push(ph);
                //Saving to Database
                let obj = { line_number: textbyline.length, phrase: ph };
                quiry.create(obj);
                var sprat = textbyline.join('\n');
                fs.writeFile('storage.txt', sprat, (err) => {
                    resolve({
                        "id": textbyline.length,
                        "phrase": ph
                    });
                });
            });
        });
    }
    getAll() {
        return new Promise((resolve, reject) => {
            fs.readFile('storage.txt', "utf8", function (err, data) {
                if (err) {
                    reject(err);
                }
                var textbyline = data.split('\n');
                var json_temp = [];
                for (let i = 0; i < textbyline.length; i++) {
                    let obj = { id: i + 1, phrase: textbyline[i] };
                    json_temp.push(obj);
                }
                resolve(json_temp);
            });
        });
    }
    deleteById(data) {
        return new Promise((resolve, reject) => {
            const remove_phrase = parseInt(data);
            fs.readFile('storage.txt', "utf8", function (err, data) {
                if (err) {
                    reject(err);
                }
                var textbyline = data.split('\n');
                //    var json_temp = [];
                //    for(let i=0; i< textbyline.length; i++){
                //     let obj:Phrase = {id: i+1, phrase:textbyline[i]};
                //     json_temp.push(obj);
                //    }
                if (isEmpty(textbyline)) {
                    var res = JSON.parse('{"error":"no phrase found with line no."}');
                    resolve(res);
                }
                else {
                    var flag = false;
                    textbyline.forEach((element, index) => {
                        if (index + 1 == remove_phrase) {
                            flag = true;
                            textbyline.splice(index, 1);
                            fs.writeFile('storage.txt', textbyline.join("\n"), (err) => {
                                if (err) {
                                    reject(err);
                                }
                            });
                            let res = JSON.parse('{"success":"Phrase delete successfully"}');
                            resolve(res);
                        }
                    });
                    if (!flag) {
                        let res = JSON.parse('{"error":"no phrase found with line no."}');
                        resolve(res);
                    }
                }
            });
        });
    }
}
exports.default = Repository;
function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
function writefile(filename) {
    let instream = fs.createReadStream(filename);
    let r1 = readline.createInterface({
        input: instream,
        output: process.stdout
    });
    r1.on("line", (line) => {
        line_list.push(line);
    });
}
//# sourceMappingURL=repository.js.map