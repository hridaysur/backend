"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Boom = require("boom");
const utils_1 = require("../helper/utils");
const logger_1 = require("../helper/logger");
class CrudController {
    constructor(crudResolver) {
        this.crudResolver = crudResolver;
        this.create = (request, response) => __awaiter(this, void 0, void 0, function* () {
            try {
                logger_1.default.info(`POST - ${utils_1.default.getUrl(request)}`);
                const data = yield this.crudResolver.save(JSON.stringify(request.payload, null, 4));
                return response.response({
                    statusCode: 200,
                    data: {
                        id: data['id'],
                    },
                });
            }
            catch (error) {
                return response.response(Boom.badImplementation(error));
            }
        });
        this.getAll = (request, response) => __awaiter(this, void 0, void 0, function* () {
            try {
                logger_1.default.info(`GET - ${utils_1.default.getUrl(request)}`);
                const entities = yield this.crudResolver.getAll();
                return response.response({
                    statusCode: 200,
                    phrases: entities,
                });
            }
            catch (error) {
                return response.response(Boom.badImplementation(error));
            }
        });
        this.deleteByID = (request, response) => __awaiter(this, void 0, void 0, function* () {
            try {
                logger_1.default.info(`DELETE - ${utils_1.default.getUrl(request)}`);
                const entities = yield this.crudResolver.deleteById(request.params._id);
                return response.response({
                    statusCode: 200,
                    data: entities,
                });
            }
            catch (error) {
                return response.response(Boom.badImplementation(error));
            }
        });
    }
}
exports.default = CrudController;
//# sourceMappingURL=crud-controller.js.map