"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    status: {
        path: '/status',
        options: {
            title: 'API Monitor',
            routeConfig: {
                auth: false,
            },
        },
    },
};
//# sourceMappingURL=config.js.map