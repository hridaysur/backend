// import * as Knex from "knex";
exports.up = (Knex) => {
    return Promise.all([Knex.schema.createTable('phrases', (table) => {
            table.increments();
            table.text('line_number');
            table.text('phrase');
        })
    ]);
};
exports.down = function (knex) {
    return Promise.all([
        knex.schema.dropTable('phrases')
    ]);
};
//# sourceMappingURL=create_phrase.js.map