"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Hapi = require("hapi");
const DotEnv = require("dotenv");
const logger_1 = require("./src/helper/logger");
const router_1 = require("./router");
class Server {
    static start() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                DotEnv.config({
                    path: `${process.cwd()}/.env`,
                });
                Server._instance = new Hapi.Server({
                    host: process.env.HOST,
                    port: process.env.PORT,
                });
                logger_1.default.info(`the server host ${process.env.HOST}`);
                logger_1.default.info(`the server port ${process.env.PORT}`);
                // await Plugin.registerAll(Server._instance);
                yield router_1.default.loadRoutes(Server._instance);
                yield Server._instance.start();
                logger_1.default.info(`Server - Up and running!`);
                return Server._instance;
            }
            catch (error) {
                logger_1.default.info(`Server - There was something wrong: ${error}`);
                throw error;
            }
        });
    }
    static instance() {
        return Server._instance;
    }
    static inject(options) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield Server._instance.inject(options);
        });
    }
}
exports.default = Server;
//# sourceMappingURL=server.js.map