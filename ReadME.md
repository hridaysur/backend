Setup a web server using Node.js, Typescript. Specifically use Hapi.js (https://hapijs.com/) as
your webserver. The web server will do two simple things:

    ● Write a phrase to the file, as sent in an http request.
    ● Read all phrases from the file, and return them in the body of an http response.
    ● Delete a single phrase from the file, if completed successfully.


Task Expectations:
1. Code should be written in typescript and transpiled to javascript. Include you
   tsconfig.json file.
2. Create one separate project using using npm locally, and utilize npm, package.json etc.
   to install packages, and have npm scripts ready to start up the webserver to do its work.
   The package should be named ‘backend’. Your evaluator should be able to download
   your git repo and run two commands to get this all working locally:
    a. npm install
    b. npm run startServer
3. Setup webserver to run on http://localhost:8080.
4. Create file called ‘storage.txt’ which holds a file that has one phrase per line.
5. Create three endpoints on the server that take http requests:
    a. /write:
i. The write endpoint will accept POST requests and take a single JSON
   parameter, ‘phrase’ and write it at the end of the file. The post body would
   be of the form:
   {phrase: “<phrase>”}
ii. The response should return the line number in the file where the response
   was written of the form:
   {id: <line_number>}
b. /read: The read endpoint will accept GET requests and return each line number
   and phrase pair in the list as a single JSON object of the form:
   {phrases: [
   {id: 1, phrase: “<phrase1>”},
   {id: 2, phrase: “<phrase2>”}, ,
    …
   {id: <line_number_n>, phrase: “<phrasen>”},
   ]}
c. /delete/<line_number>:
i. The delete endpoint will accept DELETE requests with the line number in
   the URL
ii. The delete endpoint will respond with a success or failure. A failure
    response should be provided if a record does not exist with the specified
    line number:
    {success: <true_or_false>}