import Phrase from '../model/phrase';
import PhraseResolver from './resolver';
import CrudController from '../common/crud-controller';

export default class PhraseController extends CrudController<Phrase> {
    constructor() {
        super(new PhraseResolver());
    }
}
