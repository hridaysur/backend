import * as Joi from 'joi';

export default {
    create: {
        payload: {
            phrase: Joi.string().required()
        },
    }
};
