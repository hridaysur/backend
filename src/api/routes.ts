import * as Hapi from 'hapi';

import validate from './validate';
import IRoute from '../helper/route';
import Logger from '../helper/logger';
import PhraseController from './controller';

export default class UserRoutes implements IRoute {
    public async register(server: Hapi.Server): Promise<any> {
        return new Promise(resolve => {
            Logger.info('Phrase - Start adding Phrase routes.');
            const controller = new PhraseController();

            server.route([
                {
                    method: 'POST',
                    path: '/write',
                    handler:controller.create,
                },
                {
                    method: 'GET',
                    path: '/read',
                    handler: controller.getAll,
                },
                {
                    method: 'DELETE',
                    path: '/delete/{_id}',
                    handler:controller.deleteByID
                }
            ]);

            Logger.info('Phrase - Finish adding Phrase routes.');

            resolve();
        });
    }
}
