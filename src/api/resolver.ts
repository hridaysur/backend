import Phrase from '../model/phrase';
import Resolver from '../common/resolver';
import Repository from '../common/repository';

export default class PhraseResolver extends Resolver<Phrase> {
    constructor() {
        super(new Repository());
    }
}
