
let Knex = require('./knex');

module.exports = {
    getAll(){
        return Knex('phrases');
    },

    create(phrase){
        return Knex('phrases').insert(phrase,'*');
    }
}