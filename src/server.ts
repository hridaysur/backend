import * as Hapi from 'hapi';
import * as DotEnv from 'dotenv';

import Logger from './helper/logger';
import Router from './router';

export default class Server {
    private static _instance: Hapi.Server;

    public static async start(): Promise<Hapi.Server> {
        try {
            DotEnv.config({
                path: `${process.cwd()}/.env`,
            });

            Server._instance = new Hapi.Server({
                host: process.env.HOST,
                port: process.env.PORT,
            });

            Logger.info(`the server host ${process.env.HOST}`);
            Logger.info(`the server port ${process.env.PORT}`);

            // await Plugin.registerAll(Server._instance);
            await Router.loadRoutes(Server._instance);

            await Server._instance.start();

            Logger.info(`Server - Up and running!`);

            return Server._instance;
        } catch (error) {
            Logger.info(`Server - There was something wrong: ${error}`);

            throw error;
        }
    }


    public static instance(): Hapi.Server {
        return Server._instance;
    }

    public static async inject(options: string | Hapi.ServerInjectOptions): Promise<Hapi.ServerInjectResponse> {
        return await Server._instance.inject(options);
    }
}
