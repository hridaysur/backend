import Repository from './repository';
import Phrase from '../model/phrase';

export default class CrudResolver<T> {
    constructor(protected repository: Repository<T>) {}

    public async save(data: string): Promise<Phrase> {
        return await this.repository.save(data);
    }

    public async getAll(): Promise<Phrase[]> {
        return await this.repository.getAll();
    }

    public async deleteById(data:string): Promise<T> {
        return await this.repository.deleteById(data);
    }

}
