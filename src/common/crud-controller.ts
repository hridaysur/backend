import * as Hapi from 'hapi';
import * as Boom from 'boom';

import Utils from '../helper/utils';
import Logger from '../helper/logger';
import CrudResolver from './resolver';
import Phrase from '../model/phrase';

export default class CrudController<T> {
    constructor(private crudResolver: CrudResolver<T>) {}

    public create = async (request: Hapi.Request, response: Hapi.ResponseToolkit): Promise<any> => {
        try {
            Logger.info(`POST - ${Utils.getUrl(request)}`);

            const data: any = await this.crudResolver.save(JSON.stringify(request.payload,null,4));

            return response.response({
                statusCode: 200,
                data: {
                    id: data['id'],
                },
            });
        } catch (error) {
            return response.response(Boom.badImplementation(error));
        }
    };

    public getAll = async (request: Hapi.Request, response: Hapi.ResponseToolkit): Promise<any> => {
        try {
            Logger.info(`GET - ${Utils.getUrl(request)}`);

            const entities:Phrase[] = await this.crudResolver.getAll();

            return response.response({
                statusCode: 200,
                phrases: entities,
            });
        } catch (error) {
            return response.response(Boom.badImplementation(error));
        }
    };

    public deleteByID = async (request: Hapi.Request, response: Hapi.ResponseToolkit):Promise<any> => {
        try {
            Logger.info(`DELETE - ${Utils.getUrl(request)}`);
            const entities: T = await this.crudResolver.deleteById(request.params._id);

            return response.response({
                statusCode:200,
                data:entities,
            });
            
        } catch (error) {
            return response.response(Boom.badImplementation(error));
        }
    }
}
